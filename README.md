# This is an exercise from the rust book covering rust 2018

This is supposed to be a text interface enabling one to manage employes of departments

## todos

- [X] Associate a string to a command 
- [X] Create a department
- [X] Create an employee
- [X] Add an employee to a department
- [X] list people from a department
- [X] list all people alphabetically
