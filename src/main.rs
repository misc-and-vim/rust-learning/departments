
use std::collections::HashMap;
use std::io;

#[derive(Debug)]
struct Department{
    name: String,
    employees: Vec<Person>,
}

#[derive(Debug)]
struct Person{
    name: String,
}

#[derive(Debug)]
struct Company {
    departments: HashMap<String, Department>,
}

impl Company {

    //pub fn new(departments: HashMap<String, Department>) -> Self {
        //Company { departments }
    //}

    pub fn default() -> Self {
        Company {departments: HashMap::<String, Department>::new()}
    }

    pub fn add_department(&mut self, department: Department) {
        self.departments.insert(String::from(&department.name), department);
    }

    pub fn add_department_from_stdin(&mut self) {
        println!("You are about to add a department to the commpany");
        self.add_department(Department::new_from_stdin());
    }

    pub fn print_departments(&self) {
        for (k, _) in &self.departments {
            println!("{}", k);
        }
    }
}

impl Department {

    //pub fn new(name: String, employees: Vec<Person>) -> Self {
        //Self { name, employees }
    //}

    //pub fn default() -> Self {
        //Self { 
            //name: String::from("Default"),
            //employees: Vec::<Person>::new(),
        //}
    //}

    pub fn new_from_stdin() -> Self{
        println!("Please enter the name of the department");
        let mut name = String::new();
        io::stdin().read_line(&mut name)
            .expect("Failed to read from stdin");
        let name = String::from(name.trim());
        Department {
            name,
            employees : vec![],
        }
    }

    pub fn add_employee(&mut self, person : Person) {
        self.employees.push(person);
    }

    pub fn add_employee_from_stdin(&mut self) {
        println!("You are about to add an employee to {}", self.name);
        self.add_employee(Person::new_from_stdin());
    }

}

impl Person {

    //pub fn new(name: String) -> Self {
        //Self { name }
    //}

    //pub fn default() -> Self {
        //Self { name: String::from("Default") }
    //}

    fn new_from_stdin() -> Self {
        println!("Please enter the name of the person");
        let mut name = String::new();
        io::stdin().read_line(&mut name)
            .expect("Failed to read from stdin");
        let name = String::from(name.trim());
        Person {
            name,
        }
    }

}


fn main_menu(mut company: Company) {
    loop {
        println!("This is the main menu");
        println!("Enter q to quit");
        let mut line = String::new();
        io::stdin().read_line(&mut line)
            .expect("Failed to read from stdin");
        let line = line.trim();
        match line {
            "add_department" => {
                company.add_department_from_stdin();
            },
            "q" => return,
            "add_employee" => {
                println!("Here is the list of departments:");
                company.print_departments();
                println!("Please enter the name of the department you want to add an employee to");
                loop {
                    let mut line = String::new();
                    io::stdin().read_line(&mut line)
                        .expect("Failed to read from stdin");
                    let line = String::from(line.trim());
                    match company.departments.get_mut(&line) {
                        Some(d) => {
                           d.add_employee_from_stdin(); 
                           break;
                        },
                        None => {
                            println!("Department not found, please try again");
                            continue;
                        },
                    }
                }
            },
            "list_everything" => {
                company.print_departments();
                for (name, department) in &company.departments {
                    println!("{}", name);
                    for person in &department.employees {
                        println!("\t{}", person.name)
                    }
                }
            },
            "list_department" => {
                println!("Here is the list of departments:");
                company.print_departments();
                loop {
                    let mut line = String::new();
                    io::stdin().read_line(&mut line)
                        .expect("Failed to read from stdin");
                    let line = String::from(line.trim());
                    match company.departments.get_mut(&line) {
                        Some(d) => {
                            for employee in &d.employees {
                                println!("{}", employee.name);
                            }
                            break;
                        },
                        None => {
                            println!("Department not found, please try again");
                            continue;
                        },
                    }
                }
            }
            "list_all" => {
                let mut all_employees = Vec::<&Person>::new();
                for (name, department) in &company.departments {
                    all_employees.extend(department.employees.iter());
                }
                all_employees.sort_by(|p, p2| *(&p.name.cmp(&p2.name)));
                for employee in all_employees {
                    println!("{}", employee.name);
                }
            }
            _ => println!("Command not recognized"),
        }
    }
}

fn main() {
    let company = Company::default();
    main_menu(company);
}
